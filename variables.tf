# terraform.tfvars data
variable "tenancy_ocid" {}
variable "compartment_ocid" {}
variable "ssh_public_key" {}

#Availability Domain option
variable "AD" {
    default="2"
}

# Virtual Cloud Network (VCN) data
variable "vcn_cidr" {
    default = "10.0.0.0/24"
}

variable "vcn_dns_label" {
    default = "vcn01"
    description = "DNS Label for VCN"
}

variable "dns_label" {
    default =  "subnet"
    description = "DNS Label for Subnet"
}

# Load Balancer details


# Operating System (OS) image

variable "operating_system_image" {
    default = "Oracle Linux"
}

variable "operating_system_image_version" {
    default = "7.9"
}

# Instanse Shape and Size
variable "instance_shape" {
    default = "VM.Standard.E2.1.Micro"
    description = "Size of Instance"
}

# Instance Image id details
variable "image_id" {
    default = "Oracle-Linux-7.9-2021.10.20-0"
}

# Other Variables
variable "region" {
	default = "uk-london-1"
}

variable "user_ocid" {
	default =  "ocid1.user.oc1..aaaaaaaagoe6oexlr7pr7lyd2tegyutmwuyhddgzeu66yisapqgbrikok4ia"
}

variable "fingerprint" {
	default = "bb:73:63:44:64:64:37:00:58:1f:05:09:5e:fe:50:39"
}

variable "private_key_path" {
	default = "~/identity_keys/oracleidentitycloudservice_asesha-11-14-13-15.pem"
}
variable "ssh_public_key2" {
	default =  "~/id_rsa.pub"
}
