# Get list availabe instance images
data "oci_core_images" "compute_images" {
	compartment_id			= var.compartment_ocid
	operating_system		= var.operating_system_image
	operating_system_version	= var.operating_system_image_version
	shape				= var.instance_shape
	sort_by				= "TIMECREATED"
	sort_order			= "DESC"
}
