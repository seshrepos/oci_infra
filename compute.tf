# data
data "oci_identity_availability_domains" "ads" {
  compartment_id = var.compartment_ocid
}
# Compute (Application service resource details)
resource "oci_core_instance" "app01" {
    availability_domain = data.oci_identity_availability_domains.ads.availability_domains[var.AD - 1].name
    compartment_id      = var.compartment_ocid
    display_name        = "Application server 1"
    shape               = var.instance_shape

    create_vnic_details {
        subnet_id       = oci_core_subnet.subnet.id
        display_name    = "Application server 01"
    }

    source_details  {
        source_type             = "image"
        source_id               = lookup(data.oci_core_images.compute_images.images[0],"id")
        boot_volume_size_in_gbs = "50"
    }

    metadata = {
	ssh_authorized_keys = chomp(file(var.ssh_public_key2))
    }

}
# Compute (Application service resource details)
resource "oci_core_instance" "app02" {
    availability_domain = data.oci_identity_availability_domains.ads.availability_domains[var.AD - 1].name
    compartment_id      = var.compartment_ocid
    display_name        = "Application server 2"
    shape               = var.instance_shape

    create_vnic_details {
        subnet_id       = oci_core_subnet.subnet.id
        display_name    = "Application server 02"
    }

    source_details  {
        source_type             = "image"
        source_id               = lookup(data.oci_core_images.compute_images.images[0],"id")
        boot_volume_size_in_gbs = "50"
    }

    metadata = {
	ssh_authorized_keys = chomp(file(var.ssh_public_key2))
    }

}
