# Resource details for Virtul Cloud Network (VCN)
resource "oci_core_virtual_network" "vcn" {
    compartment_id      = var.compartment_ocid
    cidr_block          = var.vcn_cidr
    dns_label           = var.vcn_dns_label
    display_name        = var.vcn_dns_label 
}

# Resource details for Internet Gateway (IGW)
resource "oci_core_internet_gateway" "igw" {
    compartment_id      = var.compartment_ocid
    vcn_id              = oci_core_virtual_network.vcn.id
    display_name        = "${var.vcn_dns_label}igw"
}

# Resource details for Route table
resource "oci_core_route_table" "PubRT" {
    compartment_id      = var.compartment_ocid
    vcn_id              = oci_core_virtual_network.vcn.id
    display_name        = "${var.vcn_dns_label}igw"

    route_rules {
        destination         = "0.0.0.0/0"
        network_entity_id   = oci_core_internet_gateway.igw.id
    }

}

# Resource for Security List
resource "oci_core_security_list"  "securitylist" {
    display_name    = "Public_SecurityList"
    compartment_id  = var.compartment_ocid
    vcn_id          = oci_core_virtual_network.vcn.id

    egress_security_rules {
        protocol    = "all"
        destination = "0.0.0.0/0"
    }

    ingress_security_rules {
        protocol    = 6
        source      = "0.0.0.0/0"

        tcp_options {
            min = 80
            max = 80
        }
    }
    ingress_security_rules {
        protocol    = 6
        source      = "0.0.0.0/0"

        tcp_options {
            min = 22
            max = 22
        }
    }
}

# Resource Details for Subnet
resource "oci_core_subnet" "subnet" {
    #availability_domain = " "
    compartment_id      = var.compartment_ocid
    vcn_id              = oci_core_virtual_network.vcn.id
    cidr_block          = cidrsubnet(var.vcn_cidr,2,1)
    display_name        = var.vcn_dns_label
    dns_label           = var.vcn_dns_label
    route_table_id      = oci_core_route_table.PubRT.id

}
